package si.trjanci.revolutcurrencyconverter.data.remote

import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query
import si.trjanci.revolutcurrencyconverter.model.remote.GetCurrenciesResponse

interface ApiService {
    @GET("/latest")
    fun getCurrencies(@Query("base") base : String) : Deferred<GetCurrenciesResponse>

}