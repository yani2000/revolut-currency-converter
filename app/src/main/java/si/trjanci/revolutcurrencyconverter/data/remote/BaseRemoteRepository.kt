package si.trjanci.revolutcurrencyconverter.data.remote

import kotlinx.coroutines.Deferred
import org.koin.core.KoinComponent
import org.koin.core.inject
import retrofit2.HttpException
import si.trjanci.revolutcurrencyconverter.model.remote.BaseResponse
import si.trjanci.revolutcurrencyconverter.util.printStacktrace
import kotlin.contracts.ExperimentalContracts

@ExperimentalContracts
abstract class BaseRemoteRepository<R : BaseResponse, DH> : RemoteRepositoryContract, KoinComponent {
    val service : ApiService by inject()

    @ExperimentalContracts
    suspend fun getDataFromWeb(deferred: Deferred<R>, dataSource: RemoteLoadCallback<DH>) {
        try {
            dataSource.onLoadIndicator(true)

            val actualResult: R? = deferred.await()

            dataSource.onLoadIndicator(false)

            actualResult?.let {
                dataSource.onSuccess(handleActualResult(actualResult))
            } ?: run {
                dataSource.onUnknownError()
            }
        } catch (e: HttpException) {
            dataSource.onLoadIndicator(false)
            dataSource.onNoServer()
        } catch (e: Throwable) {
            e.message?.let { printStacktrace(e) }
            dataSource.onLoadIndicator(false)
            dataSource.onUnknownError()
        }
    }

    abstract fun handleActualResult(result : R) : DH

}