package si.trjanci.revolutcurrencyconverter.data.remote

import si.trjanci.revolutcurrencyconverter.model.ui.MainDataHolder
import kotlin.contracts.ExperimentalContracts

@ExperimentalContracts
interface RemoteRepositoryContract {

    interface MainRemoteRepositoryContract {
        suspend fun getCurrencies(base : String, callback: RemoteLoadCallback<MainDataHolder>)
    }


}