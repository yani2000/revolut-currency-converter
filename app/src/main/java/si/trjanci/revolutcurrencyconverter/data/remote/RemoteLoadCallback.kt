package si.trjanci.revolutcurrencyconverter.data.remote

interface RemoteLoadCallback<DH> {
    fun onSuccess(data: DH)
    fun onUnknownError()
    fun onNoInternet()
    fun onNoServer()
    fun onLoadIndicator(active: Boolean)
}