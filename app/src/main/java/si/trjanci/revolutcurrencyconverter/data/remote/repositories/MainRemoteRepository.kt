package si.trjanci.revolutcurrencyconverter.data.remote.repositories

import si.trjanci.revolutcurrencyconverter.data.remote.BaseRemoteRepository
import si.trjanci.revolutcurrencyconverter.data.remote.RemoteLoadCallback
import si.trjanci.revolutcurrencyconverter.data.remote.RemoteRepositoryContract
import si.trjanci.revolutcurrencyconverter.model.remote.GetCurrenciesResponse
import si.trjanci.revolutcurrencyconverter.model.ui.MainDataHolder
import kotlin.contracts.ExperimentalContracts

@ExperimentalContracts
class MainRemoteRepository: BaseRemoteRepository<GetCurrenciesResponse, MainDataHolder>(),
    RemoteRepositoryContract.MainRemoteRepositoryContract {

    override suspend fun getCurrencies(base: String, callback: RemoteLoadCallback<MainDataHolder>) {
        getDataFromWeb(service.getCurrencies(base), callback)
    }

    override fun handleActualResult(result: GetCurrenciesResponse): MainDataHolder {
        return MainDataHolder(result.rates.toList() as ArrayList<Pair<String, Double>>)
    }




}