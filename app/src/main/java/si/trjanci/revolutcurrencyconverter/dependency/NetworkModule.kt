package si.trjanci.revolutcurrencyconverter.dependency

import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import si.trjanci.revolutcurrencyconverter.BuildConfig
import si.trjanci.revolutcurrencyconverter.common.Constants
import si.trjanci.revolutcurrencyconverter.data.remote.ApiService
import si.trjanci.revolutcurrencyconverter.util.ApiLogger
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

val networkModule = module {

    single{ GsonBuilder().create()}

    single<Interceptor> {
        val logging = HttpLoggingInterceptor(ApiLogger())
        logging.level = HttpLoggingInterceptor.Level.BODY
        logging
    }

    single {
        val httpClient = OkHttpClient.Builder()
        httpClient.connectTimeout(Constants.CONNECTION_TIMEOUT, TimeUnit.SECONDS)
        httpClient.writeTimeout(Constants.CONNECTION_TIMEOUT, TimeUnit.SECONDS)
        httpClient.readTimeout(Constants.CONNECTION_TIMEOUT, TimeUnit.SECONDS)
        httpClient.addInterceptor(get())
        httpClient.build()
    }

    single {
        Retrofit.Builder().baseUrl(BuildConfig.SERVICE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .client(get())
            .callbackExecutor(Executors.newCachedThreadPool())
            .build()
    }

    single {
        (get() as Retrofit).create(ApiService::class.java)
    }
}