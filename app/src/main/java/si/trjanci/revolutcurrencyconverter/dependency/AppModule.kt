package si.trjanci.revolutcurrencyconverter.dependency

import org.koin.dsl.module
import org.koin.experimental.builder.singleBy
import si.trjanci.revolutcurrencyconverter.data.remote.repositories.MainRemoteRepository
import si.trjanci.revolutcurrencyconverter.data.remote.RemoteRepositoryContract
import kotlin.contracts.ExperimentalContracts

@ExperimentalContracts
val appModule = module {
    singleBy<RemoteRepositoryContract.MainRemoteRepositoryContract, MainRemoteRepository>()

}