package si.trjanci.revolutcurrencyconverter.dependency

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import org.koin.experimental.builder.factory
import si.trjanci.revolutcurrencyconverter.domain.CurrencyConverterUseCase
import si.trjanci.revolutcurrencyconverter.ui.MainViewModel
import kotlin.contracts.ExperimentalContracts

@ExperimentalContracts
val activityModule = module {
    factory<CurrencyConverterUseCase>()

    viewModel { MainViewModel(get()) }

}