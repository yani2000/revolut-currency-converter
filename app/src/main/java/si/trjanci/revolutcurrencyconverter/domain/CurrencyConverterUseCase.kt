package si.trjanci.revolutcurrencyconverter.domain

import si.trjanci.revolutcurrencyconverter.data.remote.RemoteLoadCallback
import si.trjanci.revolutcurrencyconverter.data.remote.RemoteRepositoryContract
import si.trjanci.revolutcurrencyconverter.model.ui.MainDataHolder
import kotlin.contracts.ExperimentalContracts

@ExperimentalContracts
class CurrencyConverterUseCase(private val remoteRepository: RemoteRepositoryContract.MainRemoteRepositoryContract) {

    suspend fun getCurrencies(base : String, callback: RemoteLoadCallback<MainDataHolder>) {
        remoteRepository.getCurrencies(base.toUpperCase(), callback)
    }

    fun calculateRates(input : MainDataHolder, currentBase : String, currentAmount : Double) : MainDataHolder {
        input.original = ArrayList(input.list)
        input.original.add(0, Pair(currentBase, 1.0))
        for (i in 0 until input.list.count()) {
            input.list[i] = Pair(input.list[i].first, input.list[i].second * currentAmount)
        }
        input.list.add(0, Pair(currentBase, currentAmount))
        return input
    }

}