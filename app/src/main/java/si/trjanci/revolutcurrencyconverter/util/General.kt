package si.trjanci.revolutcurrencyconverter.util

inline fun <reified T> T.TAG(): String = T::class.java.simpleName