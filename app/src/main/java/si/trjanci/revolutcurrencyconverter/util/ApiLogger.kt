package si.trjanci.revolutcurrencyconverter.util

import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import com.google.gson.JsonSyntaxException
import okhttp3.logging.HttpLoggingInterceptor

class ApiLogger : HttpLoggingInterceptor.Logger {

    companion object {
        fun printMessage(logName: String, message: String) {
            if (message.startsWith("{") || message.startsWith("[")) {
                try {
                    val json = GsonBuilder().setPrettyPrinting().create().toJson(JsonParser().parse(message))
                    printVerbose(logName, json)
                } catch (ex: JsonSyntaxException) {
                    printError(logName, message)
                }
            } else {
                printVerbose(logName, message)

            }
        }
    }

    override fun log(message: String) {
        printMessage("ApiLogger", message)
    }
}