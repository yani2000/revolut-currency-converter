package si.trjanci.revolutcurrencyconverter.util

import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity

fun AppCompatActivity.extToaster(@StringRes text : Int) {
    Toast.makeText(this, text, Toast.LENGTH_LONG).show()
}
