package si.trjanci.revolutcurrencyconverter.util

import android.util.Log
import si.trjanci.revolutcurrencyconverter.BuildConfig

var DO_PRINT_LOG: Boolean = BuildConfig.DEBUG

fun setPrintLog(printLog: Boolean) {
    DO_PRINT_LOG = printLog
}

fun printError(TAG: String, message: String) {
    if (!DO_PRINT_LOG) return
    Log.e(TAG, message)
}

fun printStacktrace(message: Throwable) {
    if (!DO_PRINT_LOG) return
    message.printStackTrace()
}

fun printVerbose(TAG: String, message: String) {
    if (!DO_PRINT_LOG) return
    Log.v(TAG, message)
}