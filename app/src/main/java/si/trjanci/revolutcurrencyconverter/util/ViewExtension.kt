package si.trjanci.revolutcurrencyconverter.util

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes

fun View.extInflateView(@LayoutRes layoutResource : Int, container: ViewGroup, attachToParentRoot : Boolean = false) : View {
    val layoutInflater = LayoutInflater.from(context)
    return layoutInflater.inflate(layoutResource, container, attachToParentRoot)
}


