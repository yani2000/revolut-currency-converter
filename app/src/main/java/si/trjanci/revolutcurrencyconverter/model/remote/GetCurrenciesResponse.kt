package si.trjanci.revolutcurrencyconverter.model.remote

@Suppress("unused")
data class GetCurrenciesResponse (
    val base: String,
    val date: String,
    val rates: Map<String, Double>
) : BaseResponse()