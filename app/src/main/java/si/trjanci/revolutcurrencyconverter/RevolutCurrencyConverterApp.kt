package si.trjanci.revolutcurrencyconverter;

import android.app.Application
import com.blongho.country_data.World
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import si.trjanci.revolutcurrencyconverter.dependency.activityModule
import si.trjanci.revolutcurrencyconverter.dependency.appModule
import si.trjanci.revolutcurrencyconverter.dependency.networkModule
import kotlin.contracts.ExperimentalContracts

@Suppress("unused")
class RevolutCurrencyConverterApp : Application() {

    @ExperimentalContracts
    override fun onCreate() {
        super.onCreate()

        World.init(this)

        // Start Koin
        startKoin {
            androidContext(this@RevolutCurrencyConverterApp)
            androidLogger(Level.DEBUG)
            modules(
                listOf(
                    appModule,
                    networkModule,
                    activityModule
                )
            )
        }
    }
}
