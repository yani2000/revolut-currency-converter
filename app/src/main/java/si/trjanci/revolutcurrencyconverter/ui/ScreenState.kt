package si.trjanci.revolutcurrencyconverter.ui

sealed class ScreenState <out T> {
    class Loading(val active : Boolean) : ScreenState<Nothing>()
    class Render<T>(val renderState: T) : ScreenState<T>()
    class Errors(val error : ResponseState) : ScreenState<Nothing>()
}