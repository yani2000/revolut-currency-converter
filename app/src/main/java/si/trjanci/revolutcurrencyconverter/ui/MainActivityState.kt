package si.trjanci.revolutcurrencyconverter.ui

import si.trjanci.revolutcurrencyconverter.model.ui.MainDataHolder

sealed class MainActivityState {
    class LoadCurrencies(val data : MainDataHolder) : MainActivityState()

}