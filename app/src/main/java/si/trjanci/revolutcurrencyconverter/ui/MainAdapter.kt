package si.trjanci.revolutcurrencyconverter.ui

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.blongho.country_data.World
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_main.view.*
import si.trjanci.revolutcurrencyconverter.R
import si.trjanci.revolutcurrencyconverter.model.ui.MainDataHolder
import si.trjanci.revolutcurrencyconverter.util.extInflateView


class MainAdapter(
    private var data : MainDataHolder,
    private val callback: (Pair<String, Double>, Boolean) -> Unit)
    : RecyclerView.Adapter<MainAdapter.ViewHolder>() {
    private var requestFocus = false
    private var updateAll = false
    private var c : Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        c = parent.context
        return ViewHolder(parent.extInflateView(R.layout.item_main, parent))
    }

    override fun getItemCount(): Int = data.list.count()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data.list[position])
    }

    private fun swapItems(start: Int) {
        requestFocus = true
        updateAll = true
        data.list[0] = data.list[start].also { data.list[start] = data.list[0] }
        notifyItemMoved(start, 0)
    }

    fun update(newData : MainDataHolder) {
        this.data = newData
        if (updateAll) {
            updateAll = false
            notifyItemRangeChanged(0, data.list.count())
        }
        else {
            notifyItemRangeChanged(1, data.list.count() - 1)
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        TextWatcher,
        View.OnClickListener,
        View.OnFocusChangeListener {
        fun bind(pair: Pair<String, Double>) = with(itemView) {
            setOnClickListener {
                swapItems(adapterPosition)
                callback(pair, true)
            }

            val country = World.getCountryFromCurrencyCode(pair.first)
            Picasso.with(c).load(country.flagResource).transform(CircleTransformation()).into(ivFlag)

            tvCurrency.text = pair.first
            tvDescription.text = country.currency.name

            etAmount.setText(pair.second.toString())

            if (adapterPosition == 0) {
                etAmount.setSelection(etAmount.text.length)
                if (requestFocus) {
                    requestFocus = false
                    etAmount.post { etAmount.requestFocus() }
                }
                etAmount.addTextChangedListener(this@ViewHolder)
                etAmount.setOnClickListener(this@ViewHolder)
                etAmount.onFocusChangeListener = this@ViewHolder

            }

        }

        override fun afterTextChanged(e: Editable?) {
            if (adapterPosition == 0) {
                try {
                    val rate = if (e.toString().isEmpty()) 0.0 else e.toString().toDouble()
                    data.list[0] = Pair(data.list[0].first, rate)
                    for (i in 1 until data.list.count()) {
                        data.list[i] = Pair(data.list[i].first, (data.original[i].second * rate))
                    }
                    callback(data.list[0], false)
                    itemView.post{ notifyItemRangeChanged(1, data.list.count() - 1) }
                }
                catch (ignore : Exception) {}
            }
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun onClick(p0: View?) {
            handleEtAmount(p0)
        }

        override fun onFocusChange(p0: View?, p1: Boolean) {
            handleEtAmount(p0)
        }

        private fun handleEtAmount(p0: View?) {
            if (p0?.id == R.id.etAmount && adapterPosition == 0) {
                itemView.etAmount.isCursorVisible = false
                itemView.etAmount.post {
                    itemView.etAmount.setSelection(itemView.etAmount.text.length)
                    itemView.etAmount.isCursorVisible = true
                }
            }
        }
    }
}