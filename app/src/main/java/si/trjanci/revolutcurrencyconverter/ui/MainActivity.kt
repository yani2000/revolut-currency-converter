package si.trjanci.revolutcurrencyconverter.ui

import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.viewModel
import si.trjanci.revolutcurrencyconverter.R
import si.trjanci.revolutcurrencyconverter.model.ui.MainDataHolder
import si.trjanci.revolutcurrencyconverter.util.TAG
import si.trjanci.revolutcurrencyconverter.util.extToaster
import kotlin.contracts.ExperimentalContracts

@ExperimentalContracts
class MainActivity : AppCompatActivity() {
    private val viewmodel: MainViewModel by viewModel()
    private var mainAdapter : MainAdapter? = null
    private var savedState : Parcelable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val itemAnimation = DefaultItemAnimator()
        itemAnimation.changeDuration = 0
        itemAnimation.moveDuration = 500
        itemAnimation.addDuration = 500
        itemAnimation.removeDuration = 500
        rvMain.layoutManager = LinearLayoutManager(this)
        rvMain.itemAnimator = itemAnimation

        viewmodel.screenState.observe(::getLifecycle, ::updateUI)
        viewmodel.loadingState.observe(::getLifecycle, ::updateUI)

    }

    private fun updateUI(screenState: ScreenState<MainActivityState>?){
        when (screenState) {
            is ScreenState.Loading -> setLoadingIndicator(screenState.active)
            is ScreenState.Render -> processRenderState(screenState.renderState)
            is ScreenState.Errors -> processErrors(screenState.error)
        }
    }

    override fun onResume() {
        Log.e(TAG(), "onResume(): viewmodel.stopCurrent = ${viewmodel.stopCurrent}")
        super.onResume()
        viewmodel.start = true
        if (viewmodel.stopCurrent) {
            viewmodel.stopCurrent = false
        }
        else {
            viewmodel.getCurrencies()
        }
        //viewmodel.getCurrencies()
    }

    override fun onPause() {
        Log.e(TAG(), "onPause(): viewmodel.start = false; viewmodel.stopCurrent = true")
        viewmodel.start = false
        viewmodel.stopCurrent = true
        super.onPause()
    }

    private fun setLoadingIndicator(@Suppress("UNUSED_PARAMETER") active: Boolean) {
        // No loader needed!
    }

    private fun showNoInternet() {
        extToaster(R.string.error_no_internet)
    }

    private fun showNoServer() {
        extToaster(R.string.error_no_service)
    }

    private fun showUnknownError() {
        extToaster(R.string.error_unknown)
    }

    private fun processRenderState(renderState: MainActivityState) {
        when (renderState) {
            is MainActivityState.LoadCurrencies -> {
                showCurrencies(renderState.data)
            }
        }
    }

    private fun processErrors(error : ResponseState) {
        when (error) {
            ResponseState.NO_INTERNET -> showNoInternet()
            ResponseState.NO_SERVER -> showNoServer()
            ResponseState.UNKNOWN_ERROR -> showUnknownError()
            else -> showUnknownError()
        }
    }

    private fun showCurrencies(data : MainDataHolder) {
        mainAdapter?.let {
            savedState = rvMain.layoutManager?.onSaveInstanceState()
            it.update(data)
            rvMain.layoutManager?.onRestoreInstanceState(savedState)
        } ?: run {
            mainAdapter = MainAdapter(data) { pair, mustUpdate -> onPairChange(pair, mustUpdate) }
            rvMain.adapter = mainAdapter
        }

    }

    private fun onPairChange(pair : Pair<String, Double>, mustUpdate : Boolean) {
        viewmodel.currentPair = pair
        if (mustUpdate) {
            viewmodel.stopCurrent = true
            viewmodel.getCurrencies()
            rvMain.scrollToPosition(0)
        }
        savedState = rvMain.layoutManager?.onSaveInstanceState()
    }

}
