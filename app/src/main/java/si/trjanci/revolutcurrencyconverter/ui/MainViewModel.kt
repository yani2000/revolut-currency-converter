package si.trjanci.revolutcurrencyconverter.ui

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import org.koin.core.KoinComponent
import si.trjanci.revolutcurrencyconverter.data.remote.RemoteLoadCallback
import si.trjanci.revolutcurrencyconverter.domain.CurrencyConverterUseCase
import si.trjanci.revolutcurrencyconverter.model.ui.MainDataHolder
import si.trjanci.revolutcurrencyconverter.util.TAG
import kotlin.contracts.ExperimentalContracts
import kotlin.coroutines.CoroutineContext

@ExperimentalContracts
class MainViewModel(private val useCase: CurrencyConverterUseCase) : ViewModel(), KoinComponent, CoroutineScope {
    private val sleep = 1000L
    private lateinit var _screenState: MutableLiveData<ScreenState<MainActivityState>>
    private lateinit var _loadingState: MutableLiveData<ScreenState<MainActivityState>>
    var start = true
    var stopCurrent = false
    var currentPair = Pair("EUR", "100".toDouble())

    init {
        start = true
    }

    private fun postScreenState(screenState: MainActivityState) {
        if (!::_screenState.isInitialized) {
            _screenState = MutableLiveData()
        }
        _screenState.postValue(ScreenState.Render(screenState))
    }

    private fun showLoading(active: Boolean) {
        if (!::_loadingState.isInitialized) {
            _loadingState = MutableLiveData()
        }
        _loadingState.postValue(ScreenState.Loading(active))
    }

    private fun postErrorState(error : ResponseState) {
        if (!::_screenState.isInitialized) {
            _screenState = MutableLiveData()
        }
        _screenState.postValue(ScreenState.Errors(error))
    }

    val screenState: LiveData<ScreenState<MainActivityState>>
        get() {
            if (!::_screenState.isInitialized) {
                _screenState = MutableLiveData()
            }
            return _screenState
        }

    val loadingState: LiveData<ScreenState<MainActivityState>>
        get() {
            if (!::_loadingState.isInitialized) {
                _loadingState = MutableLiveData()
            }
            return _loadingState
        }

    private lateinit var job: Job

    // Coroutines
    override val coroutineContext: CoroutineContext
        get() {
            if (!::job.isInitialized || job.isCancelled) {
                job = Job()
            }
            return Dispatchers.Main + job
        }

    private val scope: CoroutineScope by lazy {
        CoroutineScope(coroutineContext)
    }

    override fun onCleared() {
        Log.e(TAG(), "onCleared()")
        if (::job.isInitialized)
            job.cancel()
        super.onCleared()
    }

    fun getCurrencies() {
        Log.e(TAG(), "getCurrencies()")
        // Background Thread
        scope.launch(Dispatchers.IO) {
            useCase.getCurrencies(currentPair.first, object : RemoteRepositoryCallback() {
                override fun onSuccess(data: MainDataHolder) {
                    postScreenState(
                        MainActivityState.LoadCurrencies(
                            useCase.calculateRates(data, currentPair.first, currentPair.second)
                        )
                    )
                    runBlocking {
                        Log.e(TAG(), "getCurrencies(): start = $start")
                        if (start) {
                            delay(sleep)
                            Log.e(TAG(), "getCurrencies(): stopCurrent = $stopCurrent")
                            if (stopCurrent) {
                                stopCurrent = false
                            } else {
                                getCurrencies()
                            }
                        }
                    }
                }
            })
        }
    }

    abstract inner class RemoteRepositoryCallback : RemoteLoadCallback<MainDataHolder> {
        override fun onUnknownError() {
            postErrorState(ResponseState.UNKNOWN_ERROR)
        }

        override fun onNoInternet() {
            postErrorState(ResponseState.NO_INTERNET)
        }

        override fun onNoServer() {
            postErrorState(ResponseState.NO_SERVER)
        }

        override fun onLoadIndicator(active: Boolean) {
            showLoading(active)
        }
    }

}