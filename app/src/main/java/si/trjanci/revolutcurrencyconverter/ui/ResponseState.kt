package si.trjanci.revolutcurrencyconverter.ui

enum class ResponseState {
    LOADING,
    NOT_LOADING,
    SUCCESS,
    UNKNOWN_ERROR,
    NO_INTERNET,
    NO_SERVER,
}